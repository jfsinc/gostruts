package simpledb

import (
	"gostruts/db"
)

// Controller is lightweight, simple database with container.
type Controller struct {
	container *Container
}

// New factories a Controller with initialized Container.
func New() Controller {
	db := Controller{}
	db.container = &Container{}
	db.Reset()
	return db
}

// Reset initializes the container values.
func (c Controller) Reset() error {
	return c.container.Initialize()
}

// GetContainer retrieves the data container.
func (c Controller) GetContainer() db.Container {
	return c.container
}
