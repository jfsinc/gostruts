package simpledb

import "fmt"

// KeyExistsError is an error that occurs when key already exists in database.
type KeyExistsError struct {
	key string
}

func (e *KeyExistsError) Error() string {
	return fmt.Sprintf("Key exists.")
}

func (e *KeyExistsError) GetKey() string {
	return e.key
}

// KeyNotExistsError is an error that occurs when key does not exist in database.
type KeyNotExistsError struct {
	key string
}

func (e *KeyNotExistsError) Error() string {
	return fmt.Sprintf("Key does not exist.")
}

func (e *KeyNotExistsError) GetKey() string {
	return e.key
}
