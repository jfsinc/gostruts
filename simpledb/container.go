package simpledb

import (
	"sync"
)

// Container is a simple map of key:value pairs.
type Container struct {
	values map[string]interface{}
}

// Storing values in a map requires locking/unlocking since maps are not thread-safe.
var mutex sync.Mutex

// Add adds a key/value to container.
// Value must not already exist at key.
func (c *Container) Add(key string, value interface{}) error {
	mutex.Lock()
	defer mutex.Unlock()

	if _, exists := c.values[key]; !exists {
		c.values[key] = value
		return nil
	}

	return &KeyExistsError{key}
}

// Get retrieves a value for key from container.
func (c Container) Get(key string) (interface{}, error) {
	if value, exists := c.values[key]; exists {
		return value, nil
	}

	return nil, &KeyNotExistsError{key}
}

// Remove removes a key/value from container.
func (c *Container) Remove(key string) error {
	mutex.Lock()
	defer mutex.Unlock()

	if _, exists := c.values[key]; exists {
		delete(c.values, key)
		return nil
	}

	return &KeyNotExistsError{key}
}

// Set sets a key/value to container.
// If value exists for key, it will be overwritten.
func (c *Container) Set(key string, value interface{}) error {
	mutex.Lock()
	defer mutex.Unlock()

	c.values[key] = value
	return nil
}

// Initialize initializes the values map.
func (c *Container) Initialize() error {
	mutex.Lock()
	defer mutex.Unlock()

	c.values = make(map[string]interface{})
	// In the future, we will return error for reset lock.
	return nil
}
