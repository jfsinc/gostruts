package simpledb

import (
	. "gopkg.in/check.v1"
)

type ErrorsSuite struct{}

var _ = Suite(&ErrorsSuite{})

func (s *ErrorsSuite) TestKeyExistsError(c *C) {
	key := "my key"
	e := &KeyExistsError{key}
	c.Assert(e, ErrorMatches, "Key exists.")
	c.Assert(e.GetKey(), Equals, key)
}

func (s *ErrorsSuite) TestKeyNotExistsError(c *C) {
	key := "my key"
	e := &KeyNotExistsError{key}
	c.Assert(e, ErrorMatches, "Key does not exist.")
	c.Assert(e.GetKey(), Equals, key)
}
