package simpledb

import (
	"gostruts/db"

	. "gopkg.in/check.v1"
)

type ControllerSuite struct {
	db Controller
}

var _ = Suite(&ControllerSuite{})

func (s *ControllerSuite) SetUpTest(c *C) {
	s.db = New()
}

func (s *ControllerSuite) TestNewSimpleDb(c *C) {
	var controller db.Controller
	var container db.Container

	c.Assert(s.db, Implements, &controller)
	c.Assert(s.db.GetContainer(), NotNil)
	c.Assert(s.db.GetContainer(), Implements, &container)
}

func (s *ControllerSuite) TestAdd(c *C) {
	c.Assert(s.db.GetContainer().Add("key", "value"), IsNil)
}

func (s *ControllerSuite) TestDoubleAdd(c *C) {
	c.Assert(s.db.GetContainer().Add("key", "value"), IsNil)
	c.Assert(s.db.GetContainer().Add("key", "value"), ErrorMatches, "Key exists.")
}

func (s *ControllerSuite) TestAddGet(c *C) {
	c.Assert(s.db.GetContainer().Add("key", "value"), IsNil)
	read, err := s.db.GetContainer().Get("key")
	c.Assert(err, IsNil)
	c.Assert(read.(string), Equals, "value")
}

func (s *ControllerSuite) TestGetNotExists(c *C) {
	value, err := s.db.GetContainer().Get("key")
	c.Assert(value, IsNil)
	c.Assert(err, ErrorMatches, "Key does not exist.")
}

func (s *ControllerSuite) TestSetGet(c *C) {
	s.db.GetContainer().Set("key", "value")
	read, err := s.db.GetContainer().Get("key")
	c.Assert(err, IsNil)
	c.Assert(read.(string), Equals, "value")
}

func (s *ControllerSuite) TestDoubleSetGet(c *C) {
	s.db.GetContainer().Set("key", "value")
	s.db.GetContainer().Set("key", "value2")
	read, err := s.db.GetContainer().Get("key")
	c.Assert(err, IsNil)
	c.Assert(read.(string), Equals, "value2")
}

func (s *ControllerSuite) TestAddSet(c *C) {
	c.Assert(s.db.GetContainer().Add("key", "value"), IsNil)
	s.db.GetContainer().Set("key", "value2")
	read, err := s.db.GetContainer().Get("key")
	c.Assert(err, IsNil)
	c.Assert(read.(string), Equals, "value2")
}

func (s *ControllerSuite) TestRemoveNotExists(c *C) {
	c.Assert(s.db.GetContainer().Remove("key"), ErrorMatches, "Key does not exist.")
}

func (s *ControllerSuite) TestAddRemoveGet(c *C) {
	c.Assert(s.db.GetContainer().Add("key", "value"), IsNil)
	c.Assert(s.db.GetContainer().Remove("key"), IsNil)
	_, err := s.db.GetContainer().Get("key")
	c.Assert(err, ErrorMatches, "Key does not exist.")
}

func (s *ControllerSuite) TestAddDoubleRemove(c *C) {
	c.Assert(s.db.GetContainer().Add("key", "value"), IsNil)
	c.Assert(s.db.GetContainer().Remove("key"), IsNil)
	c.Assert(s.db.GetContainer().Remove("key"), ErrorMatches, "Key does not exist.")
}

func (s *ControllerSuite) TestSetGetResetGet(c *C) {
	s.db.GetContainer().Set("key", "value")
	read, err := s.db.GetContainer().Get("key")
	c.Assert(err, IsNil)
	c.Assert(read.(string), Equals, "value")
	c.Assert(s.db.Reset(), IsNil)
	_, err = s.db.GetContainer().Get("key")
	c.Assert(err, ErrorMatches, "Key does not exist.")
}
