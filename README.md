# Overview #

Gostruts is a collection of common packages and dependencies for other projects.

# Db #
Db defines interfaces for basic database containers and controllers.

# Simpledb #

Simpledb implements Db interfaces for simple add/get/set/remove datastore functionality.