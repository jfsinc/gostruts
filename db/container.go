package db

// Container is a generic store of key/value pairs.
type Container interface {
	// Create a new key/value pair. It must not exist.
	Add(string, interface{}) error
	// Create a key/value pair. Existing values are overwritten.
	Set(string, interface{}) error
	// Get a key's value. It must exist.
	Get(string) (interface{}, error)
	// Remove a key/value pair. It must exist.
	Remove(string) error
	// Initialize all values in container.
	Initialize() error
}
