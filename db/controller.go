package db

// Controller is a generic database mechanism for managing access to a keys and values.
type Controller interface {
	// Remove all key/value pairs from the container.
	Reset() error
	// Get the container that provides key/value storage.
	GetContainer() Container
}
